namespace :alexa do
  desc "TODO"
  task scrape_sites: :environment do
    session = Ralexa.session(ENV["AWS_KEY"], ENV["AWS_SECRET"])
    global = session.top_sites.global(100)
    global.each do |site|
      website = Website.find_or_create_by(url: site.url)
      website.rank = site.rank
      website.pageviews_per_user = site.page_views_per_user
      website.save
    end
  end

end
