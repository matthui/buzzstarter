class CreateWebsites < ActiveRecord::Migration
  def change
    create_table :websites do |t|
      t.string :url
      t.integer :rank
      t.decimal :pageviews_per_user
      t.timestamps null: false
    end
  end
end
