$(document).on('ready page:load', function() {
  $('body').on('click', '.websites td', function() {
    $('#modal').modal('show')
    var url = "/websites/" + $(this).parent().data('id')
    $('form').attr('action', url)
    $.get(url, function( data ) {
      $('#website_rank').val(data.rank)
      $('#website_url').val(data.url)
      $('#website_pageviews_per_user').val(data.pageviews_per_user)
    });
    
  })
}) 