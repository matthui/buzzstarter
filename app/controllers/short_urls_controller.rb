class ShortUrlsController < ApplicationController

  def index
    @urls = ShortUrl.all
  end

  def show
    @url = ShortUrl.find(params[:id])
    @url.clicks.create(ip: request.remote_ip)
    redirect_to @url.url
  end

  def create
    @url = ShortUrl.new(url_params)
    @url.save
    redirect_to short_urls_path(@url)
  end

  def info
    @url = ShortUrl.find(params[:id])
  end

  private

    def url_params
      params.require(:short_url).permit(:url)
    end
end