class WebsitesController < ApplicationController

  def index
    @order_by = params[:order] || 'rank'
    @websites = Website.order("#{@order_by} asc").page(params[:page]).per(25)
  end

  def show
    @website = Website.find(params[:id])
    render :json => @website
  end

  def update
    @website = Website.find(params[:id])
    @website.update_attributes(website_params)
    respond_to do |format|   
      format.html    
      format.js    
    end
  end

  private

    def website_params
      params.require(:website).permit(:url, :rank, :pageviews_per_user)
    end

end